package org.eocencle.magnet.xmlbuilder.builder;

import org.eocencle.magnet.core.mapping.InfoParam;
import org.eocencle.magnet.core.util.StrictMap;
import org.eocencle.magnet.xmlbuilder.parsing.XNode;
import org.eocencle.magnet.xmlbuilder.session.XmlProjectConfig;
import org.eocencle.magnet.xmlbuilder.util.XMLBuilderTag;

import java.util.ArrayList;
import java.util.List;

/**
 * 变量建构类
 * @author: huan
 * @Date: 2020-01-17
 * @Description:
 * 变量构建顺序：
 * 1、搜索到value值的时候，构造简单变量
 * 2、搜索到ref值的时候，构造引用变量
 * 3、搜索包含list列表的时候，构造列表变量
 * 4、搜索包含map映射额时候，构造映射变量
 */
public class VariableBuilder implements XMLParser {
    // 单例实体
    private static VariableBuilder BUILDER = new VariableBuilder();

    private XmlProjectConfig xmlProjectConfig;

    private VariableBuilder() {

    }

    /**
     * 获取单例实体
     * @Author huan
     * @Date 2020-01-18
     * @Param []
     * @Return org.eocencle.magnet.builder.xml.VariableBuilder
     * @Exception
     * @Description
     **/
    public static VariableBuilder getInstance() {
        return BUILDER;
    }

    @Override
    public void parse(XNode node, XmlProjectConfig config) {
        List<XNode> nodes = node.getChildren();
        this.xmlProjectConfig = config;
        for (XNode n: nodes) {
            // 1、搜索到value值的时候，构造简单变量
            if (null != n.getStringAttribute(XMLBuilderTag.XML_ATTR_VALUE, null)) {
                config.putParameterInfo(n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY),
                        n.getStringAttribute(XMLBuilderTag.XML_ATTR_VALUE));
            }
            // 2、搜索到ref值的时候，构造引用变量
            else if (null != n.getStringAttribute(XMLBuilderTag.XML_ATTR_REF, null)) {
                Object ref = config.getParameterInfo(n.getStringAttribute(XMLBuilderTag.XML_ATTR_REF));
                config.putParameterInfo(n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY), ref);
            }
            // 3、搜索包含list列表的时候，构造列表变量
            else if (null != n.evalNode(XMLBuilderTag.XML_EL_LIST)) {
                config.putParameterInfo(n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY),
                        this.parseList(n.evalNode(XMLBuilderTag.XML_EL_LIST)));
            }
            // 4、搜索包含map映射额时候，构造映射变量
            else if (null != n.evalNode(XMLBuilderTag.XML_EL_MAP)) {
                config.putParameterInfo(n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY),
                        this.parseMap(n.evalNode(XMLBuilderTag.XML_EL_MAP)));
            }
        }
    }

    /**
     * 解析参数元素
     * @Author huan
     * @Date 2020-05-06
     * @Param [nodes]
     * @Return org.eocencle.magnet.core.util.StrictMap<org.eocencle.magnet.core.mapping.InfoParam>
     * @Exception
     * @Description
     */
    public StrictMap<InfoParam> parseElements(List<XNode> nodes) {
        StrictMap<InfoParam> result = new StrictMap<>("params");
        for (XNode n: nodes) {
            // 1、搜索到value值的时候，构造简单变量
            if (null != n.getStringAttribute(XMLBuilderTag.XML_ATTR_VALUE, null)) {
                result.put(n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY), this.parseParamSingle(n));
            }
            // 2、搜索到ref值的时候，构造引用变量
            else if (null != n.getStringAttribute(XMLBuilderTag.XML_ATTR_REF, null)) {

            }
            // 3、搜索包含list列表的时候，构造列表变量
            else if (null != n.evalNode(XMLBuilderTag.XML_EL_LIST)) {
                result.put(n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY), this.parseParamList(n));
            }
            // 4、搜索包含map映射额时候，构造映射变量
            else if (null != n.evalNode(XMLBuilderTag.XML_EL_MAP)) {
                result.put(n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY), this.parseParamMap(n));
            }
        }
        return result;
    }

    /**
     * 解析list变量
     * @Author huan
     * @Date 2020-03-07
     * @Param [node]
     * @Return java.util.List<java.lang.String>
     * @Exception
     * @Description
     **/
    private List<String> parseList(XNode node) {
        List<String> list = new ArrayList<>();
        for (XNode n: node.getChildren()) {
            list.add(n.getStringBody());
        }
        return list;
    }

    /**
     * 解析map变量
     * @Author huan
     * @Date 2020-03-07
     * @Param [node]
     * @Return org.eocencle.magnet.util.StrictMap<java.lang.String>
     * @Exception
     * @Description
     **/
    private StrictMap<String> parseMap(XNode node) {
        StrictMap<String> map = new StrictMap<String>("Custom map");
        for (XNode n: node.getChildren()) {
            map.put(n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY), n.getStringAttribute(XMLBuilderTag.XML_ATTR_VALUE));
        }
        return map;
    }

    /**
     * 解析单一参数
     * @Author huan
     * @Date 2020-05-06
     * @Param [node]
     * @Return org.eocencle.magnet.core.mapping.InfoParam
     * @Exception
     * @Description
     */
    private InfoParam parseParamSingle(XNode node) {
        return new InfoParam(node.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY),
                node.getStringAttribute(XMLBuilderTag.XML_ATTR_VALUE));
    }

    /**
     * 解析列表参数
     * @Author huan
     * @Date 2020-05-06
     * @Param [node]
     * @Return org.eocencle.magnet.core.mapping.InfoParam
     * @Exception
     * @Description
     */
    private InfoParam parseParamList(XNode node) {
        List<String> list = new ArrayList<>();
        for (XNode n: node.evalNode(XMLBuilderTag.XML_EL_LIST).getChildren()) {
            list.add(n.getStringBody());
        }
        InfoParam infoParam = new InfoParam();
        infoParam.setKey(node.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY));
        infoParam.setList(list);
        return infoParam;
    }

    /**
     * 解析映射参数
     * @Author huan
     * @Date 2020-05-06
     * @Param [node]
     * @Return org.eocencle.magnet.core.mapping.InfoParam
     * @Exception
     * @Description
     */
    private InfoParam parseParamMap(XNode node) {
        StrictMap<InfoParam> map = new StrictMap<>("Map params");
        for (XNode n: node.evalNode(XMLBuilderTag.XML_EL_MAP).getChildren()) {
            String key = n.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY);
            map.put(key, new InfoParam(key, n.getStringAttribute(XMLBuilderTag.XML_ATTR_VALUE)));
        }
        InfoParam infoParam = new InfoParam();
        infoParam.setKey(node.getStringAttribute(XMLBuilderTag.XML_ATTR_KEY));
        infoParam.setMap(map);
        return infoParam;
    }
}

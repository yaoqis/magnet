package org.eocencle.magnet.core.mapping;

/**
 * 重分区信息类
 * @author: huan
 * @Date: 2021-05-07
 * @Description:
 */
public class RepartitionInfo extends WorkFlowInfo {
    // 别名
    private String alias;
    // 引用
    private String ref;
    // 分片数
    private Integer num;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}

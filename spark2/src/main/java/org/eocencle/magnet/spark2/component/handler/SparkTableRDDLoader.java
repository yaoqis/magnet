package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.mapping.DataSourceField;
import org.eocencle.magnet.core.mapping.TableInfo;
import org.eocencle.magnet.core.util.StrictMap;

/**
 * Spark表RDD优先加载器抽象类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public abstract class SparkTableRDDLoader extends SparkTableLoader {

    public SparkTableRDDLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [context, src]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    public abstract JavaRDD<Row> createRDD(SparkContext context, String src);

    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-15
     * @Param [session, dst, rdd]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    public abstract Dataset<Row> createDataFrame(SparkSession session, StrictMap<DataSourceField> dst, JavaRDD<Row> rdd);

}

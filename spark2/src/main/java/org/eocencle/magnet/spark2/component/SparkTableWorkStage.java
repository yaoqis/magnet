package org.eocencle.magnet.spark2.component;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.component.*;
import org.eocencle.magnet.core.context.ComponentFactory;
import org.eocencle.magnet.core.context.Context;
import org.eocencle.magnet.spark2.component.handler.SparkTableDataFrameLoader;
import org.eocencle.magnet.spark2.component.handler.SparkTableLoader;
import org.eocencle.magnet.spark2.component.handler.SparkTableRDDLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Spark表作业节点类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkTableWorkStage extends TableWorkStage {
    // 表加载器
    private SparkTableLoader tableLoader;

    @Override
    public void initHandler(WorkStageHandler handler) {
        this.tableLoader = (SparkTableLoader) handler;
    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        ComponentFactory factory = WorkStageComponentBuilderAssistant.getFactory();
        Context context = parameter.getContext();

        JavaRDD<Row> rdd = null;
        Dataset<Row> ds = null;
        if (this.tableLoader instanceof SparkTableRDDLoader) {
            SparkTableRDDLoader loader = (SparkTableRDDLoader) this.tableLoader;
            // 创建RDD
            rdd = loader.createRDD((SparkContext) context.getContext(), this.tableInfo.getSrc());
            // 创建Dataset
            ds = loader.createDataFrame((SparkSession) context.getSQLContext(), this.tableInfo.getFields(), rdd);
        } else {
            SparkTableDataFrameLoader loader = (SparkTableDataFrameLoader) this.tableLoader;
            // 创建Dataset
            ds = loader.createDataFrame(context, this.tableInfo.getSrc());
            // 创建RDD
            rdd = loader.createRDD(ds);
        }

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.tableInfo.getId());
        result.setAlias(this.tableInfo.getAlias());
        result.setRdd(rdd);
        result.setDs(ds);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }
}

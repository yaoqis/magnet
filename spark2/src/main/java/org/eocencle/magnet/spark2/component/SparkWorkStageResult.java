package org.eocencle.magnet.spark2.component;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.component.WorkStageResult;

/**
 * Spark作业结果类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkWorkStageResult extends WorkStageResult {
    // spakr rdd
    private JavaRDD<Row> rdd;
    // spark ds
    private Dataset<Row> ds;
    // 表名
    private String tableName;
    // 注册后表名
    private String regTableName;

    public JavaRDD<Row> getRdd() {
        return rdd;
    }

    public void setRdd(JavaRDD<Row> rdd) {
        this.rdd = rdd;
    }

    public Dataset<Row> getDs() {
        return ds;
    }

    public void setDs(Dataset<Row> ds) {
        this.ds = ds;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getRegTableName() {
        return regTableName;
    }

    public void setRegTableName(String regTableName) {
        this.regTableName = regTableName;
    }
}

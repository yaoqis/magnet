package org.eocencle.magnet.spark2.component.handler.impl;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.context.Context;
import org.eocencle.magnet.core.mapping.TableInfo;
import org.eocencle.magnet.spark2.component.handler.SparkTableDataFrameLoader;

/**
 * SparkRC格式表作业节点类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkRCTableLoader extends SparkTableDataFrameLoader {

    public SparkRCTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    @Override
    public JavaRDD<Row> createRDD(Dataset<Row> ds) {
        return null;
    }

    @Override
    public Dataset<Row> createDataFrame(Context context, String src) {
        return null;
    }

}

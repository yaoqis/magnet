package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.context.Context;
import org.eocencle.magnet.core.mapping.TableInfo;

/**
 * Spark表DF优先加载器抽象类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public abstract class SparkTableDataFrameLoader extends SparkTableLoader {

    public SparkTableDataFrameLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [ds]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    public abstract JavaRDD<Row> createRDD(Dataset<Row> ds);

    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-15
     * @Param [context, src]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    public abstract Dataset<Row> createDataFrame(Context context, String src);

}

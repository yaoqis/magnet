package org.eocencle.magnet.spark2.component.handler.impl;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.context.Context;
import org.eocencle.magnet.core.mapping.TableInfo;
import org.eocencle.magnet.spark2.component.handler.SparkTableDataFrameLoader;

/**
 * SparkParquet格式表作业节点类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkParquetTableLoader extends SparkTableDataFrameLoader {

    public SparkParquetTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [ds]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    public JavaRDD<Row> createRDD(Dataset<Row> ds) {
        return ds.toJavaRDD();
    }

    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-15
     * @Param [context, src]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    public Dataset<Row> createDataFrame(Context context, String src) {
        return ((SparkSession) context.getSQLContext()).read().parquet(src);
    }
}

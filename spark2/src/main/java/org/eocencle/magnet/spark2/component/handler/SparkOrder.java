package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.OrderInfo;

/**
 * Spark排序器
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkOrder extends WorkStageHandler {
    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-16
     * @Param [ds]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    JavaRDD<Row> createRDD(Dataset<Row> ds);
    /**
     * 创建DataFrame
     * @Author huan
     * @Date 2020-08-15
     * @Param [orderInfo, df]
     * @Return org.apache.spark.sql.DataFrame
     * @Exception
     * @Description
     **/
    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-16
     * @Param [orderInfo, ds]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    Dataset<Row> createDataset(OrderInfo orderInfo, Dataset<Row> ds);
}

package org.eocencle.magnet.spark2.component.handler.impl;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.context.Context;
import org.eocencle.magnet.core.mapping.TableInfo;
import org.eocencle.magnet.spark2.component.handler.SparkTableDataFrameLoader;

/**
 * SparkAvro格式表作业节点类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkAvroTableLoader extends SparkTableDataFrameLoader {

    public SparkAvroTableLoader(TableInfo tableInfo) {
        super(tableInfo);
    }

    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [ds]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    @Override
    public JavaRDD<Row> createRDD(Dataset<Row> ds) {
        return ds.toJavaRDD();
    }

    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-15
     * @Param [context, src]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    @Override
    public Dataset<Row> createDataFrame(Context context, String src) {
        ((SparkContext) context.getContext()).hadoopConfiguration()
                .set("avro.mapred.ignore.inputs.without.extension", "false");
        return ((SparkSession) context.getSQLContext()).read().format("com.databricks.spark.avro").load(src);
    }
}

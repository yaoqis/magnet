package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.AddSequenceInfo;
import org.eocencle.magnet.spark2.component.SparkWorkStageResult;

/**
 * Spark添加序列处理器接口
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkAddSequenceHandler extends WorkStageHandler {
    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [prevResult, addSequenceInfo]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    JavaRDD<Row> createRDD(SparkWorkStageResult prevResult, AddSequenceInfo addSequenceInfo);
    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-16
     * @Param [session, rdd]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    Dataset<Row> createDataFrame(SparkSession session, JavaRDD rdd);
}

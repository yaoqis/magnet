package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.AddFieldsInfo;
import org.eocencle.magnet.spark2.component.SparkWorkStageResult;

/**
 * Spark添加字段处理器接口
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkAddFieldsHandler extends WorkStageHandler {
    /**
     * 创建RDD
     * @Author huan
     * @Date 2020-08-15
     * @Param [prevResult, addFieldsInfo]
     * @Return org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     **/
    JavaRDD<Row> createRDD(SparkWorkStageResult prevResult, AddFieldsInfo addFieldsInfo);
    /**
     * 创建Dataset
     * @Author huan
     * @Date 2020-08-15
     * @Param [session, rdd]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    Dataset<Row> createDataFrame(SparkSession session, JavaRDD rdd);
}

package org.eocencle.magnet.spark2.component;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.component.*;
import org.eocencle.magnet.core.context.ComponentFactory;
import org.eocencle.magnet.spark2.component.handler.SparkJoinCondition;

import java.util.ArrayList;
import java.util.List;

/**
 * Spark关联作业节点类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkJoinWorkStage extends JoinWorkStage {
    // on条件
    private SparkJoinCondition sparkJoinCondition;

    @Override
    public void initHandler(WorkStageHandler handler) {
        this.sparkJoinCondition = (SparkJoinCondition) handler;
    }

    @Override
    public List<WorkStageResult> execute(WorkStageParameter parameter) {
        ComponentFactory factory = WorkStageComponentBuilderAssistant.getFactory();

        WorkStageComposite parent = this.getParent();
        Dataset<Row> left = ((SparkWorkStageResult) parent.getIdResult(this.joinInfo.getLeftRef())).getDs();
        Dataset<Row> right = ((SparkWorkStageResult) parent.getIdResult(this.joinInfo.getRightRef())).getDs();

        // 创建Dataset
        Dataset<Row> ds = this.sparkJoinCondition.on(this.joinInfo.getFilterFields(), left, right, this.joinInfo.getType());

        // 创建RDD
        JavaRDD<Row> rdd = ds.toJavaRDD();

        // 设置返回值
        SparkWorkStageResult result = (SparkWorkStageResult) factory.createWorkStageResult();
        result.setId(this.joinInfo.getId());
        result.setAlias(this.joinInfo.getAlias());
        result.setRdd(rdd);
        result.setDs(ds);
        List<WorkStageResult> list = new ArrayList<>();
        list.add(result);
        return list;
    }
}

package org.eocencle.magnet.spark2.component.handler.impl;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.mapping.ValueMappersInfo;
import org.eocencle.magnet.spark2.component.handler.SparkValueMappersHandler;

/**
 * Spark默认值映射类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkDefaultValueMappersHandler implements SparkValueMappersHandler {
    @Override
    public Dataset<Row> handle(Dataset<Row> ds, ValueMappersInfo valueMappersInfo) {

        return null;
    }
}

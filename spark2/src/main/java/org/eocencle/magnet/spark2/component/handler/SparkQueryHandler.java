package org.eocencle.magnet.spark2.component.handler;

import org.eocencle.magnet.core.component.WorkStageHandler;

/**
 * Spark查询接口
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkQueryHandler extends WorkStageHandler {
}

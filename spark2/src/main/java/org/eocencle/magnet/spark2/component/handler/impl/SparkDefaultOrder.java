package org.eocencle.magnet.spark2.component.handler.impl;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.mapping.OrderInfo;
import org.eocencle.magnet.core.util.CoreTag;
import org.eocencle.magnet.spark2.component.handler.SparkOrder;

/**
 * Spark默认排序类
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public class SparkDefaultOrder implements SparkOrder {
    @Override
    public JavaRDD<Row> createRDD(Dataset<Row> ds) {
        return ds.toJavaRDD();
    }

    @Override
    public Dataset<Row> createDataset(OrderInfo orderInfo, Dataset<Row> ds) {
        Dataset<Row> result = null;
        String[] fields = orderInfo.getField().split(CoreTag.SPLIT_COMMA);
        if (null != fields && 0 != fields.length) {
            Column[] columns = new Column[fields.length];
            for (int i = 0; i < fields.length; i ++) {
                columns[i] = this.setOrder(fields[i], ds);
            }
            result = ds.orderBy(columns);
        } else {
            result = ds;
        }

        return result;
    }

    /**
     * 设置排序
     * @Author huan
     * @Date 2020-08-15
     * @Param [field, ds]
     * @Return org.apache.spark.sql.Column
     * @Exception
     * @Description
     **/
    private Column setOrder(String field, Dataset<Row> ds) {
        String[] complete = field.trim().split(CoreTag.STRING_SPACE);
        Column column = ds.col(complete[0]);
        if (1 == complete.length) {
            column = column.asc();
        } else {
            if (CoreTag.ORDERBY_ASC.equals(complete[1].toLowerCase())) {
                column = column.asc();
            } else if (CoreTag.ORDERBY_DESC.equals(complete[1].toLowerCase())) {
                column = column.desc();
            }
        }
        return column;
    }
}

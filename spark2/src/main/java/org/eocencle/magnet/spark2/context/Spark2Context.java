package org.eocencle.magnet.spark2.context;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.eocencle.magnet.core.context.Context;
import org.eocencle.magnet.core.exception.UnsupportedException;
import org.eocencle.magnet.core.util.CoreTag;
import org.eocencle.magnet.extend.Register;

/**
 * Spark2.x版执行环境
 * @author: huan
 * @Date: 2020-05-25
 * @Description:
 */
public class Spark2Context extends Context {
    // spark上下文
    private SparkContext sc;
    // spark会话
    private SparkSession session;
    // sparkStream上下文
    private JavaStreamingContext ssc;
    // java spark上下文
    private JavaSparkContext jsc;

    @Override
    public void start() {
        // 环境模式判断
        if (this.getEnvMode().toUpperCase().startsWith(CoreTag.ENV_MODE_LOCAL.toUpperCase())) {
            this.session = SparkSession.builder().appName(this.getAppName()).master(this.getEnvMode().toLowerCase()).getOrCreate();
        } else {
            this.session = SparkSession.builder().appName(this.getAppName()).getOrCreate();
        }

        // 执行模式判断
        if (CoreTag.PROCESS_MODE_BATCH.equalsIgnoreCase(this.getProcessMode())) {
            this.sc = this.session.sparkContext();
        } else if (CoreTag.PROCESS_MODE_STREAM.equalsIgnoreCase(this.getProcessMode())) {
            if (CoreTag.SQL_ENGINE_SPARK.equalsIgnoreCase(this.getSqlEngine())) {

            } else if (CoreTag.SQL_ENGINE_HIVE.equalsIgnoreCase(this.getSqlEngine())) {

            } else {
                throw new UnsupportedException(this.getSqlEngine() + " SQL engine not supported");
            }
        } else {
            throw new UnsupportedException("Unknown execution mode " + this.getProcessMode());
        }

        // 注册自定义函数
        if (StringUtils.isNoneBlank(this.getExtendRegister())) {
            try {
                Class<?> regClass = Class.forName(this.getExtendRegister());
                Register register = (Register) regClass.newInstance();
                register.regSQLFunc(this.session.udf());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void stop() {
        if (CoreTag.PROCESS_MODE_BATCH.equalsIgnoreCase(this.getProcessMode())) {
            this.sc.stop();
        } else if (CoreTag.PROCESS_MODE_STREAM.equalsIgnoreCase(this.getProcessMode())) {
            this.ssc.start();
            this.ssc.close();
        }
    }

    @Override
    public Object getContext() {
        return this.sc;
    }

    @Override
    public Object getSQLContext() {
        return this.session;
    }

    @Override
    public Object getStreamContext() {
        return this.ssc;
    }
}

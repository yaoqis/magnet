package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.FilterField;

import java.util.List;

/**
 * Spark关联条件接口
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkJoinCondition extends WorkStageHandler {
    /**
     * 关联条件
     * @Author huan
     * @Date 2020-08-16
     * @Param [filterFields, left, right, type]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    Dataset<Row> on(List<FilterField> filterFields, Dataset<Row> left, Dataset<Row> right, String type);
}

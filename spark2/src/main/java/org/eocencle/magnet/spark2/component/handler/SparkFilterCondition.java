package org.eocencle.magnet.spark2.component.handler;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.eocencle.magnet.core.component.WorkStageHandler;
import org.eocencle.magnet.core.mapping.FilterField;

import java.util.List;

/**
 * Spark过滤条件接口
 * @author: huan
 * @Date: 2020-08-15
 * @Description:
 */
public interface SparkFilterCondition extends WorkStageHandler {
    /**
     * 过滤验证
     * @Author huan
     * @Date 2020-08-16
     * @Param [filterFields, ds]
     * @Return org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>
     * @Exception
     * @Description
     */
    Dataset<Row> filter(List<FilterField> filterFields, Dataset<Row> ds);
}
